public class Prep {
/* while (io.hasMoreTokens()) {
 *    int n = io.getInt();
 *    double d = io.getDouble();
 *    double ans = d*n;
 *
 *    io.println("Answer: " + ans);
 * }
 *
 * io.close();
	*/


	Kattio io = new Kattio(System.in, System.out);
	double[][] transisionMatrix;
	double[][] emissionMatrix;
	double[] initStateMatrix;
	int[] sequenceOfEmission;


	public void runTheJewls() {
		while(io.hasMoreTokens()) { 

			// Parse Transision matrix
			int rows = io.getInt();
			int cols = io.getInt();
			transisionMatrix = new double[rows][cols];
			for(int i=0; i<rows; i++) {
				for(int j=0; j<cols; j++) {
					transisionMatrix[i][j] = io.getDouble();
				}
			}

			// Parse Emission matrix
			rows = io.getInt();
			cols = io.getInt();
			emissionMatrix = new double[rows][cols];
			for(int i=0; i<rows; i++) {
				for(int j=0; j<cols; j++) {
					emissionMatrix[i][j] = io.getDouble();
				}
			}

			// Parse Initial state matrix
			rows = io.getInt();
			cols = io.getInt();
			initStateMatrix = new double[cols];

			for(int j=0; j<cols; j++) {
				initStateMatrix[j] = io.getDouble();
			}

			rows = io.getInt();
			sequenceOfEmission = new int[rows];

			for(int i=0; i<rows; i++) {
				sequenceOfEmission[i] = io.getInt();
			}

		}


		HMM model = new HMM(transisionMatrix, emissionMatrix, initStateMatrix);
		int[] res = model.estimateStateSequence(sequenceOfEmission);
		for(int x : res)
			io.print(x + " ");
		io.close();

		/*
		double[] res = model.estimateProbabilityDistributionOfNextEmission(initStateMatrix);
		io.print(1 + " " + res.length + " ");
		for(int i=0; i<res.length; i++) {
			io.print(res[i] + " ");
		}
		io.close();
		*/
		//estimateProbabilityOfEmissionSequence();


	}
	public static void main(String[] args) {
		Prep p = new Prep();
		p.runTheJewls();
	}
	
}