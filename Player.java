import java.util.*;

class Player {

    int minimumSequenceLength = 45;
    double minimumProbability = 0.6;
    HMM[] markovModels;
    int[] birdSpecies;
    HMM[] speciesModels;
    HMM storkHMM;

    public Player() {
    }

    /**
     * Shoot!
     *
     * This is the function where you start your work.
     *
     * You will receive a variable pState, which contains information about all
     * birds, both dead and alive. Each bird contains all past moves.
     *
     * The state also contains the scores for all players and the number of
     * time steps elapsed since the last time this function was called.
     *
     * @param pState the GameState object with observations etc
     * @param pDue time before which we must have returned
     * @return the prediction of a bird we want to shoot at, or cDontShoot to pass
     */

    public Action shoot(GameState pState, Deadline pDue) {
        /*
         * Here you should write your clever algorithms to get the best action.
         * This skeleton never shoots.
         */

        if(pState.getBird(0).getSeqLength() < minimumSequenceLength) {
            return cDontShoot;
        }
        // When we have 45 observations per bird
        int numBirds = pState.getNumBirds();
        markovModels = new HMM[numBirds];

        // create markovModels
        for(int i=0; i<numBirds; i++){
            Bird thisBird = pState.getBird(i);
            // init markov model for each bird
            markovModels[i] = new HMM(5, Constants.COUNT_MOVE);
        }

        // train each markov model
        for(int i=0; i<markovModels.length; i++){
            Bird thisBird = pState.getBird(i);
            if(thisBird.isDead()){
                continue;
            }
            markovModels[i].estimateModel(getObservationsOf(thisBird));
        }

        // Get bird with highest probability of getting hit
        double maxProb = 0;
        int indexOfBestBird = 0;
        int move = 0;
        for(int i=0; i<markovModels.length; i++){
            if(pState.getBird(i).isDead()){
                continue;
            }
            if(storkHMM != null && storkHMM.estimateProbabilityOfEmissionSequence(getObservationsOf(pState.getBird(i))) > 0) {
                continue;
            }
            int length = markovModels[i].gamma.length;
            double[] g = markovModels[i].gamma[length-1];

            double[] probabilityOfMoves = markovModels[i].estimateProbabilityDistributionOfNextEmission(g);
            for(int j=0; j<probabilityOfMoves.length; j++) {
                double max = probabilityOfMoves[j];
                if(max > maxProb) {
                    maxProb = max;
                    indexOfBestBird = i;
                    move = j;
                }
            }
        }

        // If best probability is lower than our threshold, dont shoot
        if(maxProb < minimumProbability) {
            return cDontShoot;
        }

        // KILL THEM LIKE PATRIC TAUGHT US
        return new Action(indexOfBestBird, move);
    }

    /**
     * Guess the species!
     * This function will be called at the end of each round, to give you
     * a chance to identify the species of the birds for extra points.
     *
     * Fill the vector with guesses for the all birds.
     * Use SPECIES_UNKNOWN to avoid guessing.
     *
     * @param pState the GameState object with observations etc
     * @param pDue time before which we must have returned
     * @return a vector with guesses for all the birds
     */
    public int[] guess(GameState pState, Deadline pDue) {
        /*
         * Here you should write your clever algorithms to guess the species of
         * each bird. This skeleton makes no guesses, better safe than sorry!
         */
        
        int[] lGuess = new int[pState.getNumBirds()];

        if(pState.getRound() == 0) {
            Random rand = new Random();
            for(int i=0; i<pState.getNumBirds(); i++){
                lGuess[i] = rand.nextInt(Constants.COUNT_SPECIES);
            }
            return lGuess;
        }

        for(int i=0; i<pState.getNumBirds(); i++){
            double maxProb = 0;
            int bestGuess = 0;
            Bird thisBird = pState.getBird(i);
            for(int k=0; k<speciesModels.length; k++){
                double prob = speciesModels[k].estimateProbabilityOfEmissionSequence(getObservationsOf(thisBird));
                if(prob > maxProb) {
                    maxProb = prob;
                    bestGuess = k;
                }
            }
            if(maxProb > 0) {
                lGuess[i] = birdSpecies[bestGuess];
            } else {
                // Gissa inte alls
                lGuess[i] = Constants.SPECIES_UNKNOWN;
                
                // Gissa slump
                //Random rand = new Random();
                //lGuess[i] = rand.nextInt(Constants.COUNT_SPECIES);
            }
        }
        return lGuess;        
    }

    /**
     * If you hit the bird you were trying to shoot, you will be notified
     * through this function.
     *
     * @param pState the GameState object with observations etc
     * @param pBird the bird you hit
     * @param pDue time before which we must have returned
     */
    public void hit(GameState pState, int pBird, Deadline pDue) {
        System.err.println("HIT BIRD!!!");
    }

    /**
     * If you made any guesses, you will find out the true species of those
     * birds through this function.
     *
     * @param pState the GameState object with observations etc
     * @param pSpecies the vector with species
     * @param pDue time before which we must have returned
     */
    public void reveal(GameState pState, int[] pSpecies, Deadline pDue) {
        if(pState.getRound() == 0){
            birdSpecies = new int[pSpecies.length];
            speciesModels = new HMM[pSpecies.length];
            for(int j=0; j<speciesModels.length; j++){
                speciesModels[j] = new HMM(5, Constants.COUNT_MOVE);
            }
            for(int i=0; i<pSpecies.length; i++) {
                birdSpecies[i] = pSpecies[i]; // Spara svaret
                Bird thisBird = pState.getBird(i);
                int[] observations = getObservationsOf(thisBird);
                speciesModels[i].estimateModel(observations);
                if(pSpecies[i] == Constants.SPECIES_BLACK_STORK){
                    storkHMM = new HMM(5, Constants.COUNT_MOVE);
                    storkHMM.estimateModel(observations);
                }
            }
        }
        if(storkHMM == null) {
            for(int i=0; i<pSpecies.length; i++){
                if(pSpecies[i] == Constants.SPECIES_BLACK_STORK){
                    Bird thisBird = pState.getBird(i);
                    storkHMM = new HMM(5, Constants.COUNT_MOVE);
                    storkHMM.estimateModel(getObservationsOf(thisBird));
                }
            }
        }
    }

    public int[] getObservationsOf(Bird thisBird) {
        int seqLength = getSequenceLengthOf(thisBird);
        int[] observations = new int[seqLength];
        for(int j=0; j<seqLength; j++){
            observations[j] = thisBird.getObservation(j);
        }
        return observations;
    }

    public int getSequenceLengthOf(Bird thisBird) {
        int seqLength = thisBird.getSeqLength();
        int toRet = 0;
        for(int i=0; i<seqLength; i++){
            if(thisBird.wasAlive(i)) {
                toRet++;
            } else {
                break;
            }
        }
        return toRet;
    }

    public static final Action cDontShoot = new Action(-1, -1);
}
